import dash
from flask import Flask
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
from dash.dependencies import Output, Input, State
import plotly.graph_objects as go
import urllib.request
import json
import datetime

external_stylesheets = [dbc.themes.BOOTSTRAP]

flask_app = Flask(__name__)
app = dash.Dash(__name__, server=flask_app, external_stylesheets=external_stylesheets)
app.title = 'PyCrypto DashBoard'

last_coin = None
act_coin_data = ([], [])


app.layout = html.Div(children=[
    dcc.Store(id='memory_list'),
    dcc.Store(id='memory_coin'),
    dcc.Store(id='memory_last_price'),
    dcc.Store(id='memory_act_price'),
    html.H1('PyCrypto DashBoard'),
    dcc.Dropdown(id='coin_selector', options=[{'label': 'BitCoin', 'value': 'BTC'},
                                              {'label': 'Ethereum', 'value': 'ETH'},
                                              {'label': 'Ripple', 'value': 'XRP'}],
                 placeholder='Select CryptoCoin',
                 value='BTC', ),
    html.Div(children=[
        dcc.Graph(id='coin_stats', animate=True),
        dcc.Interval(
            id='update-component',
            interval=60 * 1000,  # in milliseconds
            n_intervals=0
        )
    ]),
    html.Div(children=[
        html.Div(id="lst_price"),
        html.Div(id="act_price"),
        html.Div(id="diff"),
        html.Div(id="proc")
    ]),
])


@app.callback(
    Output('coin_stats', 'figure'),
    Input('memory_list', 'value'),
    State('memory_coin', 'value')
)
def set_coin_stats(lst, coin):
    fig = go.Figure()
    fig.add_trace(go.Line(x=lst[0], y=lst[1], name=coin))
    fig.update_layout(
        title=f'Current price of {coin}',
        yaxis_title=f'1 {coin} price in Euro',
        xaxis_title='Time Stamp'
    )
    return fig


@app.callback(
    Output('lst_price', 'children'),
    Output('act_price', 'children'),
    Output('diff', 'children'),
    Output('proc', 'children'),
    Input('memory_act_price', 'value'),
    State('memory_last_price', 'value')
)
def set_meta_info(act_prc, lst_prc):
    return html.Div(f'Last price: {lst_prc :.2f} €'), html.Div(f'Actual price: {act_prc :.2f} €'), html.Div(f'Difference: {act_prc - lst_prc :.2f} €'), html.Div(f'Percentage Change: {(act_prc - lst_prc) * 100 / act_prc :.4f} %')


@app.callback(
    Output('memory_list', 'value'),
    Output('memory_coin', 'value'),
    Output('memory_act_price', 'value'),
    Output('memory_last_price', 'value'),
    Input('update-component', 'n_intervals'),
    Input('coin_selector', 'value'),
    State('memory_coin', 'value'),
    State('memory_list', 'value'),
    State('memory_act_price', 'value'),
    State('memory_last_price', 'value')
)
def get_coin_stat(n, new_coin, mem_coin, mem_lst, mem_act_prc, mem_last_prc):

    if mem_coin != new_coin:
        mem_lst = ([], [])
        mem_last_prc = 0.0
        mem_coin = new_coin

    url = f'https://api.nomics.com/v1/currencies/ticker?key=ff87d730cebebc93f7f7dc75e80c9c54&ids={new_coin}&interval=1d,30d&convert=EUR&per-page=100&page=1'
    data = json.loads(urllib.request.urlopen(url).read().decode('utf-8'))[0]
    t = datetime.datetime.strptime(data["price_timestamp"], '%Y-%m-%dT%H:%M:%SZ')
    mem_last_prc = mem_act_prc
    mem_act_prc = round(float(data["price"]), 2)
    mem_lst[0].append(t)
    mem_lst[1].append(mem_act_prc)

    return mem_lst, mem_coin, mem_act_prc, mem_last_prc


if __name__ == '__main__':
    app.run_server(debug=True)
